import { Component, OnInit } from '@angular/core';
import { Articulo } from '../models/articulo';
import { Router } from '@angular/router';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.scss']
})
export class ArticuloComponent implements OnInit {

  articulos:Array<Articulo>=new Array<Articulo>();

  constructor(private ruta:Router) { }

  ngOnInit() {
    this.articulos.push(
      {
        nombre:"Televisión 24''",
        descripcion:"Marca SAMSUMG con 4 años de garantia",
        precio:1500,
        stock:10,
        preicoMayorista:1400
      },
      {
        nombre:"Laptop I5",
        descripcion:"Laptop nueva con 8GB de RAM",
        precio:15000,
        stock:20,
        preicoMayorista:14000
      },
      {
        nombre:"Monitor",
        descripcion:"Monitor curvo con pantalla fullHD",
        precio:8000,
        stock:60,
        preicoMayorista:7500
      }
    );
  }

  pasarParametro(art:Articulo){
    this.ruta.navigate(['articuloDetalle',{articulo:JSON.stringify(art)}])
  }

}
