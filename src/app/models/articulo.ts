export interface Articulo{
    nombre:string,
    descripcion:string,
    precio:number,
    stock:number,
    preicoMayorista:number
  }