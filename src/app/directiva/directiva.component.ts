import { Component, OnInit } from '@angular/core';

interface Producto{
  nombre:string,
  stock:number,
  fabricante:string,
  fechaVence:Date,
  esImportante:boolean
}

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html',
  styleUrls: ['./directiva.component.scss']
})

export class DirectivaComponent implements OnInit {

  cargando:boolean=true;
  nombres:Array<string>=["Maria","Juan","Ana","Pedro"]
  pestana:string=''
  mostrarCuadrado=false;
  productos:Array<Producto>=[
    {
      nombre:'Arroz',
      stock:15,
      fabricante:'Distribuidor Industrial',
      fechaVence:new Date('04/15/2020'), // Date(mes/dia/año)
      esImportante:true
    },
    {
      nombre:'Maíz',
      stock:25,
      fabricante:'Distribuidor Industrial',
      fechaVence:new Date('04/15/2021'),
      esImportante:false
    },
    {
      nombre:'Refresco',
      stock:200,
      fabricante:'Coca Cola',
      fechaVence:new Date('01/15/2024'),
      esImportante:true
    }
  ]

  constructor() { }

  ngOnInit() {
    setTimeout(()=>{
      this.cargando=false;
    },5000)
  }

  mostrarCargando(){
      this.cargando=!this.cargando;
  }

  cambiarPestana(pestana:string){
    this.pestana=pestana;
  }

  alternarFondo(){
    this.mostrarCuadrado=!this.mostrarCuadrado
  }

}
