import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.scss']
})
export class PipesComponent implements OnInit {

  titulo:string="Hola, soy un título";
  dinero:number=2400;
  ganancias:number=0.54;
  fechaNacimiento:Date=new Date('01/06/1997');
  textoLargo:string="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Enim consequuntur molestiae beatae autem consequatur ea, at, amet aliquam asperiores quasi sunt obcaecati praesentium placeat ullam incidunt fugiat qui hic ab."

  constructor() { }

  ngOnInit() {
  }

}