// ng generate component mi componente. 
// ng serve --o

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-titulos',
  templateUrl: './titulos.component.html',
  styleUrls: ['./titulos.component.scss']
})
export class TitulosComponent implements OnInit {
  nombre:string="Maria"
  apellido:string="Perez"
  alumno:any={
    nombre:'Carlos',
    apellido:'Pineda',
    edad:21
  }

  imagen:string="https://www.mexicodesconocido.com.mx/wp-content/uploads/2019/05/pajaro-quetzal_57d0b4db_1200x630.jpg";
  ingreso:string="Hola, ingresa algo"

  correo:string='';
  password:string='';

  constructor() { }

  ngOnInit() {
  }

  iniciar(){
    console.log(this.correo+" "+this.password);
  }

  llamarAlerta(){
    alert('Ha hecho doble click')
  }

  escribirModelo(){
    console.log(this.password);
  }

  escribir(dato:string){
    console.log(dato)
  }

  colorFondo(evento){
    evento.srcElement.style.background="#1976d2"
  }

  iniciar00(evento){
    if(evento.key=="Enter"){
      console.log("Ingresando al sistema :)")
    }
  }

  cambiarTam(evento){
    evento.srcElement.style.width="500px"
    evento.srcElement.style.heigth="200px"
    evento.srcElement.style.border="1px solid #1976d2"
  }

  incrementarTam(evento){
    evento.srcElement.style.width="300px"
    evento.srcElement.style.heigth="50px"
    evento.srcElement.style.border="1px solid #1976d2"
  }
}
